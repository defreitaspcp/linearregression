##Regressão linear[1]
Em estatística ou econometria, regressão linear é uma equação para se estimar a condicional (valor esperado) de uma variável y, dados os valores de algumas outras variáveis x.
A regressão, em geral, tem como objetivo tratar de um valor que não se consegue estimar inicialmente.

##Equação da Regressão Linear[1]
Para se estimar o valor esperado, usa-se de uma equação, que determina a relação entre ambas as variáveis, Y = alpha + beta * X + epsilon.

Em que:

Y - Variável explicada (dependente); é o valor que se quer atingir;

alpha - É uma constante, que representa a interceptação da reta com o eixo vertical;

beta - É outra constante, que representa o declive(coeficiente angular)da reta;

X - Variável explicativa (independente), representa o fator explicativo na equação;

epsilon - Variável que inclui todos os factores residuais mais os possíveis erros de medição.

##Referência

[1]Regressão linear, wikipedia, https://pt.wikipedia.org/wiki/Regress%C3%A3o_linear