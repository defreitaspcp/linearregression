/*
Author : de Freitas, P.C.P
Simple linear regression
*/
#include<iostream>
using namespace std;
double mean(double data[], int length)
{
	double sum = 0.0;
	for (int i = 0; i < length; i++)
	{
		sum += data[i];
	}
	return sum / length;
}
double var(double data[], int length)
{
	double sum = 0;
	for (int i = 0; i < length; i++)
	{
		sum += pow(data[i] - mean(data, length), 2);
	}
	return sum;
}
double cov(double a[], double b[], int length)
{
	double sum = 0.0;
	for (int i = 0; i < length; i++)
	{
		sum += (a[i] - mean(a, length))*(b[i] - mean(b, length));
	}
	return sum;
}
double beta(double x[], double y[], int length)
{
	return cov(x, y, length) / var(x, length);
}
double alpha(double x[], double y[], int length)
{
	return mean(y, length) - beta(x, y, length)*mean(x, length);
}
void display(double x[], double y[], int length)
{
	cout << "Coefficient beta : " << beta(x, y, length) << endl;
	cout << "Coefficient alfa : " << alpha(x, y, length) << endl;
	cout << "Fitting the regression line, y = "
		<< alpha(x, y, length)
		<< " + "
		<< "("
		<< beta(x, y, length)
		<< ")"
		<< "x"
		<< endl;
}
int main()
{
	double CDAM20181[24] = { 1.49,1.09,0.96,1.22,1.24,1.32,1.21,0.99,0.96,1.06,1.02,1.05,
		1.02,0.96,1.18,0.99,0.98,1.86,0.99,1.15,1.21,1.08,1.06,0.96 };
	double NEAM20181[24] = { 1.94726,1.46045,1.46045,0,1.55781,0,1.94726,1.16836,1.65517,0,1.60649,1.26572,
		0.973631,1.65517,0,1.07099,0,1.75254,1.65517,1.75254,0,0,1.07099,0 };
	display(CDAM20181, NEAM20181, 24);
	cout << endl;

	double CDAM20182[8] = { 1.02,0.99,0.84,1.06,1.02,1.02,0.99,0.99 };
	double NEAM20182[8] = { 1.07177,1.26316,1.22488,1.33971,0.995215,0.344498,1.07177,0.688995 };
	display(CDAM20182, NEAM20182, 8);
	cout << endl;

	double CDES20181[11] = { 1.18,1.15,1.09,1.09,1.09,1.08,1.05,1.02,1.02,0.99,0.99 };
	double NEES20181[11] = { 0,0,1.85393,0,1.97753,0,1.11236,1.85393,0.865169,1.60674,1.73034 };
	display(CDES20181, NEES20181, 11);
	cout << endl;

	double CDAAM20181[11] = { 1.86,1.49,1.21,1.24,0.99,1.02,0.96,0.96,1.09,0.96,1.15 };
	double NEAAM20181[11] = { 1.04485,1.16095,1.16095,0.92876,0.986807,0.957784,
		0.986807,0.986807,0.870712,0.870712,1.04485 };
	display(CDAAM20181, NEAAM20181, 11);
	cout << endl;

	double CDAAM20182[5] = { 1.06,1.02,0.99,0.84,0.99 };
	double NEAAM20182[5] = { 1.12179,0.897436,0.897436,1.02564,1.05769 };
	display(CDAAM20182, NEAAM20182, 5);
	cout << endl;

	double CDAES20181[4] = { 1.09,1.09,1.02,0.99 };
	double NEAES20181[4] = { 1.06667,1,1,0.933333 };
	display(CDAES20181, NEAES20181, 4);
	cout << endl;

	return 0;
}